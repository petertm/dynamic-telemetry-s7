# dynamic-telemetry-s7
Dynamically Generated Telemetry Display Using the Siemens S7-1200/S7-1500 Web server

This is a set of html files to be added to a a Siemens S7-1200 or S7-1500 PLC that can dynamically generate a responsive webpage showing automatically updating sensor and actuator values based on a json file

For Usage Information, check [here](https://petertm.gitlab.io/vfield.co.uk/PLCTelemetry/PLCTelemetry.html)

![](images/headerimg.jpg)


## Credits
The project contains code from the following projects.

*  [Vue](https://vuejs.org) is Copyright (c) 2013-present, Yuxi (Evan) You, Licenced under the [MIT Licence](https://opensource.org/licenses/MIThttps://opensource.org/licenses/MIT)
*  [Boostrap](https://getbootstrap.com/) is Copyright (c) 2011-2018 Twitter, Inc. and Copyright (c) 2011-2018 The Bootstrap Authors, Licenced under the [MIT Licence](https://opensource.org/licenses/MIThttps://opensource.org/licenses/MIT)

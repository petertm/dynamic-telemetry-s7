# Setting Up the Webpages

Setup is reletively simple

1.  Enable the S7 webserver in TIA portal in Device Configuration > Webserver
2.  Edit the json in **data.html** and replace the tags with data from your own PLC project. A new table on the webpage will be generated for every top level object. Each element has an array with two values. Thee first value should be a reference to the PLC tag, and the second should be the unit for that value. The unit can be left blank if not needed. 
3.  Edit the HTML in **index.html** and add/change page titles, headings and logos as desired
4.  Upload the files in the **Site** folder using the 'GENERATE WEB DB' button in TIA portal user defined pages section
5.  Upload the project to the PLC
6.  Access the user defined pages via the PLC's IP address


## Structure of the data.html file
![](images/JSOntag.png)

